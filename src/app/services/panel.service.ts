import { Injectable } from '@angular/core';
import { Subject, Subscription } from 'rxjs';

@Injectable()
export class PanelService {
  togglePanel = new Subject<any>();
  togglePanel$ = this.togglePanel.asObservable();

  public subscribeToTogglePanel(
    delegate: (togglePanel: any) => void
  ): Subscription {
    return this.togglePanel$.subscribe((togglePanel: any) => {
      delegate(togglePanel);
    });
  }
}
