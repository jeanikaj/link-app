import { Injectable } from '@angular/core';

@Injectable()
export class ProfileService {
  // Simulate getting data from a backend
  // For test purposes and the purpose of this application, this data is retrieved separately

  getUserColors(): Promise<Colors> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({ backgroundColor: '#F5F7F8', color: '#39E09B' });
      }, 300);
    });
  }

  getShows(): Promise<Ticket[]> {
    let shows;
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(
          (shows = [
            new Ticket(
              new Date('09/10/2021'),
              'The Forum',
              'Melbourne',
              true,
              'https://www.songkick.com/concerts/39894755-lorde-at-riverstage'
            ),
            new Ticket(
              new Date('09/11/2021'),
              'Hard Rock Cafe',
              'Canberra',
              false,
              '#'
            ),
            new Ticket(
              new Date('09/12/2021'),
              'The Joinery',
              'Brisbane',
              true,
              '#'
            ),
          ])
        );
      }, 300);
      return shows;
    });
  }

  getMusic(): Promise<Song> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(
          new Song('Song Name', 'Artist Name', 'assets/images/album.svg', [
            {
              id: 1,
              name: 'Spotify',
              url: 'https://www.spotify.com',
              iconUrl: 'assets/icons/spotify.svg',
            },
            {
              id: 2,
              name: 'Twitter',
              url: 'https://www.twitter.com',
              iconUrl: 'assets/icons/twitter.svg',
            },
          ])
        );
      }, 300);
    });
  }
}

// @TODO move below to models but for ease of reading I have put it below
export class Ticket {
  constructor(
    public date: Date,
    public venue: string,
    public state: string,
    public hasTickets: boolean,
    public url: string
  ) {}
}

export class Song {
  constructor(
    public songName: string,
    public artist: string,
    public artworkUrl: string,
    public platforms: Platform[]
  ) {}
}

export interface Platform {
  id: number;
  name: string;
  url: string;
  iconUrl: string;
}

export interface Colors {
  color: string;
  backgroundColor: string;
}
