import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { CustomComponentsModule } from 'src/app/components/components.module';
import { PanelService } from 'src/app/services/panel.service';
import { ClassicComponent } from './links/classic/classic.component';
import { MusicComponent } from './links/music/music.component';
import { ShowsComponent } from './links/shows/shows.component';
import { ProfileRoutingModule } from './profile-routing.module';

import { ProfileComponent } from './profile.component';
import { ProfileService } from '../../services/profile.service';

describe('ProfileComponent', () => {
  let component: ProfileComponent;
  let fixture: ComponentFixture<ProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ProfileComponent,
        ClassicComponent,
        ShowsComponent,
        MusicComponent,
      ],
      imports: [CommonModule, CustomComponentsModule, ProfileRoutingModule],
      providers: [ProfileService, PanelService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it(
    'when a user clicks on the Music Player Link and then on a Shows List Link, the Music Player Link closes',
    waitForAsync(() => {
      component.getMusic();
      component.getShows();
      fixture.detectChanges();

      fixture.whenStable().then(() => {
        // Click music link
        const musicList = fixture.debugElement.query(By.css('#musicList'));
        const musicButton = musicList.query(By.css('app-button'));
        musicButton.nativeElement.click();
        fixture.detectChanges();
        const musicVisible = musicList.query(By.css('h4'));
        expect(musicVisible.nativeElement.textContent).toContain('Spotify');

        // Click on shows link
        const showsList = fixture.debugElement.query(By.css('#showsList'));
        const showsButton = showsList.query(By.css('app-button'));
        showsButton.nativeElement.click();
        fixture.detectChanges();
        const showsVisible = showsList.query(By.css('h4'));
        expect(showsVisible.nativeElement.textContent).toContain('Sep 10 2021');

        // Check music link content is not there
        expect(musicVisible.nativeElement.style.maxHeight).toEqual('');
      });
    })
  );

  // @TODO add tests
});
