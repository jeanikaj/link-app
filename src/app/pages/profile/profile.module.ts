import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile.component';
import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileService } from '../../services/profile.service';
import { ClassicComponent } from './links/classic/classic.component';
import { MusicComponent } from './links/music/music.component';
import { ShowsComponent } from './links/shows/shows.component';
import { CustomComponentsModule } from 'src/app/components/components.module';
import { PanelService } from 'src/app/services/panel.service';

@NgModule({
  declarations: [
    ProfileComponent,
    ClassicComponent,
    ShowsComponent,
    MusicComponent,
  ],
  imports: [CommonModule, CustomComponentsModule, ProfileRoutingModule],
  providers: [ProfileService, PanelService],
})
export class ProfileModule {}
