import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-classic',
  templateUrl: './classic.component.html',
  styleUrls: ['./classic.component.scss'],
})
export class ClassicComponent {
  @Input() title: string;
  @Input() url: string;
  @Input() color: string;
  @Input() backgroundColor: string;

  constructor() {}
}
