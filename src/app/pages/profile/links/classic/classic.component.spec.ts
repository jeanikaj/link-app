import { Component, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { CustomComponentsModule } from 'src/app/components/components.module';

import { ClassicComponent } from './classic.component';

describe('ClassicComponent', () => {
  let testHostFixture: ComponentFixture<TestHostComponent>;
  let testHostComponent: TestHostComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ClassicComponent, TestHostComponent],
      imports: [CustomComponentsModule],
    }).compileComponents();
    testHostFixture = TestBed.createComponent(TestHostComponent);
    testHostComponent = testHostFixture.componentInstance;
    testHostFixture.detectChanges();
  });

  it('should create', () => {
    expect(testHostComponent).toBeTruthy();
  });

  it('should have a url if url is passed in', () => {
    expect(testHostComponent.classicComponent.url).toEqual(
      'https://linktr.ee/'
    );
  });

  it(
    'should open link in new tab',
    waitForAsync(() => {
      testHostFixture.detectChanges();
      spyOn(window, 'open');
      const link = testHostFixture.nativeElement.querySelector('a');
      link.click();
      testHostFixture.whenStable().then(() => {
        expect(window.open).toHaveBeenCalled();
        expect(window.open).toHaveBeenCalledWith(
          'https://linktr.ee/',
          '_blank'
        );
      });
    })
  );

  @Component({
    selector: `app-host-component`,
    template: `<app-classic
      [url]="'https://linktr.ee/'"
      [title]="'Linktree'"
    ></app-classic>`,
  })
  class TestHostComponent {
    @ViewChild(ClassicComponent)
    public classicComponent: ClassicComponent;
  }
});
