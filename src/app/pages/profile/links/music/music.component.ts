import { Component, Input } from '@angular/core';
import { PanelService } from 'src/app/services/panel.service';
import { LinkType } from 'src/app/enums/button-type.enum';
import { Song } from '../../../../services/profile.service';

@Component({
  selector: 'app-music',
  templateUrl: './music.component.html',
  styleUrls: ['./music.component.scss'],
})
export class MusicComponent {
  @Input() title: string;
  @Input() music: Song;
  @Input() color: string;
  @Input() backgroundColor: string;
  @Input() isActive: boolean;
  progressbarUrl = 'assets/images/progressbar.svg';

  constructor(private panelService: PanelService) {}

  onPlay(): void {
    // @TODO implement play
    console.log('play');
  }

  onClick(): void {
    this.panelService.togglePanel.next(LinkType.Music);
  }
}
