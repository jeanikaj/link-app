import { Component, Input } from '@angular/core';
import { PanelService } from 'src/app/services/panel.service';
import { LinkType } from 'src/app/enums/button-type.enum';
import { Ticket } from '../../../../services/profile.service';

@Component({
  selector: 'app-shows',
  templateUrl: './shows.component.html',
  styleUrls: ['./shows.component.scss'],
})
export class ShowsComponent {
  @Input() title: string;
  @Input() shows: Ticket[];
  @Input() imageUrl: string;
  @Input() color: string;
  @Input() backgroundColor: string;
  @Input() isActive: boolean;

  constructor(private panelService: PanelService) {}

  onClick(): void {
    this.panelService.togglePanel.next(LinkType.Shows);
  }
}
