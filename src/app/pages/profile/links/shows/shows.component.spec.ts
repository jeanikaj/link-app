import { Component, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { CustomComponentsModule } from 'src/app/components/components.module';
import { PanelService } from 'src/app/services/panel.service';
import { Ticket } from '../../../../services/profile.service';

import { ShowsComponent } from './shows.component';

describe('ShowsComponent', () => {
  let testHostFixture: ComponentFixture<TestHostComponent>;
  let testHostComponent: TestHostComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ShowsComponent, TestHostComponent],
      imports: [CustomComponentsModule],
      providers: [PanelService],
    }).compileComponents();
    testHostFixture = TestBed.createComponent(TestHostComponent);
    testHostComponent = testHostFixture.componentInstance;
    testHostFixture.detectChanges();
  });

  it('should create', () => {
    expect(testHostComponent).toBeTruthy();
  });

  it('a list of 2 shows are visible when the user clicks on the shows link', () => {
    const button = testHostFixture.nativeElement.querySelector('button');
    button.click();
    testHostFixture.detectChanges();
    const items = testHostFixture.debugElement.queryAll(
      By.css('.listItem--heading')
    );
    expect(items.length).toBe(2);
  });

  it(
    'navigates user to event in Songkick',
    waitForAsync(() => {
      const button = testHostFixture.nativeElement.querySelector('button');
      button.click();
      testHostFixture.detectChanges();
      spyOn(window, 'open');
      const link = testHostFixture.nativeElement
        .querySelector('#link0')
        .querySelector('a');
      expect(link.textContent).toContain('The Forum, Melbourne');
      link.click();
      testHostFixture.whenStable().then(() => {
        expect(window.open).toHaveBeenCalled();
        expect(window.open).toHaveBeenCalledWith(
          'https://www.songkick.com/concerts/39894755-lorde-at-riverstage',
          '_blank'
        );
      });
    })
  );

  @Component({
    selector: `app-host-component`,
    template: `<app-shows [title]="'Shows'" [shows]="shows"></app-shows>`,
  })
  class TestHostComponent {
    @ViewChild(ShowsComponent)
    public showsComponent: ShowsComponent;
    shows = [
      new Ticket(
        new Date('09/10/2021'),
        'The Forum',
        'Melbourne',
        true,
        'https://www.songkick.com/concerts/39894755-lorde-at-riverstage'
      ),
      new Ticket(
        new Date('09/11/2021'),
        'Hard Rock Cafe',
        'Canberra',
        false,
        '#'
      ),
    ];
  }
});
