import { Component, ElementRef, OnInit } from '@angular/core';
import { PanelService } from 'src/app/services/panel.service';
import { ProfileService, Song, Ticket } from '../../services/profile.service';
import { LinkType } from 'src/app/enums/button-type.enum';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  showsLabel = 'Shows'; // @TODO get from a service call
  musicLabel = 'Music'; // @TODO get from a service call
  classicLabel = 'Classic'; // @TODO get from a service call
  imageUrl: 'assets/icons/arrow.svg'; // @TODO add to a iconfont library
  shows: Ticket[];
  music: Song;
  userName = '@yourname'; // @TODO get from user profile
  backgroundColor: string;
  color: string;
  selected: Element;
  musicActive: boolean;
  showsActive: boolean;

  constructor(
    private profileService: ProfileService,
    private panelService: PanelService,
    private elem: ElementRef
  ) {}

  ngOnInit(): void {
    this.getShows();
    this.getMusic();
    this.getUserColors();
    this.panelService.subscribeToTogglePanel((val) => {
      this.toggleActive(val);
    });
  }

  getUserColors(): void {
    this.profileService.getUserColors().then((res) => {
      this.color = res.color;
      this.backgroundColor = res.backgroundColor;
    });
  }

  getShows(): void {
    this.profileService.getShows().then((res) => {
      this.shows = res;
    });
  }

  getMusic(): void {
    this.profileService.getMusic().then((res) => {
      this.music = res;
    });
  }

  toggleActive(type: LinkType): void {
    const active = this.elem.nativeElement.querySelectorAll('.active');
    // manage selected items and set active style if another panel is opened
    if ((this.selected && this.selected !== active[0]) || active.length <= 0) {
      // Clear all active classes if same link is clicked
      this.clearActiveClass(active);
      this.selected = null;
    }
    if (this.selected && active.length > 0) {
      active[0].classList.remove('.active');
      this.selected = active[1];
      console.log(this.selected);

      if (type === LinkType.Shows) {
        this.showsActive = true;
        this.musicActive = false;
      }
      if (type === LinkType.Music) {
        this.musicActive = true;
        this.showsActive = false;
      }
    } else {
      this.selected = active[0];
      if (type === LinkType.Shows) {
        this.showsActive = true;
      }
      if (type === LinkType.Music) {
        this.musicActive = true;
      }
    }
  }

  clearActiveClass(active): void {
    for (const element of active) {
      if (element) {
        element.classList.remove('.active');
        this.showsActive = false;
        this.musicActive = false;
      }
    }
  }
}
