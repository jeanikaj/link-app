import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss'],
})
export class ListItemComponent {
  @Input() heading: string;
  @Input() date: Date;
  @Input() description: string[];
  @Input() prefixImageUrl: string;
  @Input() suffixImageUrl: string;
  @Input() showAltText: boolean;
  @Input() url: string;
  @Input() showDivider: boolean;

  get formattedDescription(): string {
    if (this.description && this.description.length > 0) {
      return this.description.join(', ');
    }
  }

  constructor() {}

  onNavigate(): void {
    // Open url in new tab
    window.open(this.url, '_blank');
  }
}
