import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonComponent } from './button/button.component';
import { ListItemComponent } from './list-item/list-item.component';
import { ListPanelComponent } from './list-panel/list-panel.component';
@NgModule({
  declarations: [ButtonComponent, ListPanelComponent, ListItemComponent],
  imports: [CommonModule],
  exports: [ButtonComponent, ListPanelComponent, ListItemComponent],
})
export class CustomComponentsModule {}
