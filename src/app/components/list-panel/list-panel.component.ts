import {
  Component,
  ElementRef,
  Input,
  OnChanges,
  SimpleChanges,
  ViewChild,
} from '@angular/core';

@Component({
  selector: 'app-list-panel',
  templateUrl: './list-panel.component.html',
  styleUrls: ['./list-panel.component.scss'],
})
export class ListPanelComponent implements OnChanges {
  @Input() title: string;
  @Input() list: any[];
  @Input() color: string;
  @Input() backgroundColor: string;
  @Input() isActive: boolean;
  @ViewChild('panel', { static: true }) panel: ElementRef;

  get panelStyle(): any {
    if (this.color) {
      return { 'background-color': this.backgroundColor };
    }
  }

  constructor() {}

  ngOnChanges(changes: SimpleChanges): void {
    // Toggle panel if another panel is set active
    if (changes.isActive) {
      if (changes.isActive.currentValue === false) {
        this.togglePanel();
      }
    }
  }

  onPanelOpened(): void {
    this.togglePanel();
    this.isActive = !this.isActive;
  }

  togglePanel(): void {
    // Set panel height / hide and show
    const panel = this.panel.nativeElement;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + 'px';
    }
  }
}
