import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { ButtonComponent } from './button.component';

describe('ButtonComponent', () => {
  let component: ButtonComponent;
  let fixture: ComponentFixture<ButtonComponent>;
  let buttonElement: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonComponent);
    component = fixture.componentInstance;
    buttonElement = fixture.debugElement.query(By.css('button'));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display a button', () => {
    expect(buttonElement).toBeTruthy();
  });

  it('should have the users chosen background color', () => {
    const color = 'red';
    component.color = color;
    fixture.detectChanges();
    expect(buttonElement.nativeElement.style.backgroundColor).toBe(color);
  });

  it('should switch color on hover', () => {
    const color = 'red';
    const hoverColor = 'blue';
    component.color = color;
    component.backgroundColor = hoverColor;
    buttonElement.triggerEventHandler('mouseover', null);
    fixture.detectChanges();
    expect(buttonElement.nativeElement.style.backgroundColor).toBe(hoverColor);
  });
});
