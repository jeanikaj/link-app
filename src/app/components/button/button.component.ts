import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent {
  @Input() title: string;
  @Input() url: string;
  @Input() color: string;
  @Input() backgroundColor: string;
  @Input() isActive: boolean;

  hover = false;

  get hoverStyle(): any {
    if (this.hover) {
      return {
        'background-color': this.backgroundColor,
        border: `2px solid ${this.color}`,
      };
    }
  }

  get colorStyle(): any {
    if (this.color) {
      return { 'background-color': this.color };
    }
  }

  get activeStyle(): any {
    if (this.isActive) {
      return {
        'background-color': this.backgroundColor,
        border: `2px solid ${this.color}`,
      };
    }
  }

  get buttonStyle(): any {
    if (this.isActive) {
      return this.activeStyle;
    }
    if (this.hover) {
      return this.hoverStyle;
    }
    return this.colorStyle;
  }

  constructor() {}

  onNavigate(): void {
    window.open(this.url, '_blank');
  }
}
